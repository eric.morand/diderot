# Diderot

[![NPM version][npm-image]][npm-url] [![Coverage percentage][coveralls-image]][coveralls-url]

> Dependency Injection, _your_ way

## Highlights

### Predictable

Diderot is strictly-typed. Registering a function returns a typed Token. Getting a token returns a typed value. Replacing a token can only be done if the replacement and the replaced tokens are compatibles - i.e. if their return types are identical. With Diderot, you can't make any mistake at compile-time: you always know what you register, what you get and what you replace.

### Reliable

Diderot does not depend on experimental features, like Decorators or Metadata Reflection, that are subjects to breaking changes in the future.

### Flexible

Diderot makes no assumption on nor enforces you to a methodology. Use it your own way. Be creative. Don't stick to the consensus. Freedom of thinking is the way to enlightenment.

## Installation

`npm i diderot`

## Usage

```ts
import {Container} from "diderot";
import {deepStrictEqual} from "assert";

const container = new Container();

// register a function and get a token
const name = container.register(() => 'SpongeBob SquarePants', []);

// get a value from a token
deepStrictEqual(container.get(name), 'SpongeBob SquarePants');

// replace a token by another token
container.replace(name, () => 'Patrick Star');

deepStrictEqual(container.get(name), 'Patrick Star');

// compose with tokens
const bestFriends = container.register((
    person: string, 
    bestie: string
) => `${person} and ${bestie} are besties`, [name, name]);

deepStrictEqual(container.get(bestFriends), 'Patrick Star and Patrick Star are besties');

// compose with functions
container.replace(bestFriends, container.register((
    person: string, 
    bestie: string
) => `${person} and ${bestie} are besties`, [() => name(), () => 'Patrick Star']));

deepStrictEqual(container.get(bestFriends), 'SpongeBob SquarePants and Patrick Star are besties');

// compose with a mix of functions and tokens
container.replace(bestFriends, container.register((
    person: string, 
    bestie: string
) => `${person} and ${bestie} are besties`, [() => name(), name]));

deepStrictEqual(container.get(bestFriends), 'SpongeBob SquarePants and Patrick Star are besties');

// detect mistakes at compile-time
const number = container.register(() => 5, []); // () => number

container.replace(bestFriends, number); // TS2322: Type '() => number' is not assignable to type '() => string'.
```

## API

Read the [documentation](https://nightlycommit.gitlab.io/diderot/) for more information.

## Contributing

* Fork the main repository
* Code
* Implement tests using [tape](https://www.npmjs.com/package/tape)
* Issue a pull request keeping in mind that all pull requests must reference an issue in the issue queue

## License

Apache-2.0 © [Eric MORAND]()

[npm-image]: https://badge.fury.io/js/diderot.svg
[npm-url]: https://npmjs.org/package/diderot
[coveralls-image]: https://coveralls.io/repos/gitlab/nightlycommit/diderot/badge.svg?branch=master
[coveralls-url]: https://coveralls.io/gitlab/nightlycommit/diderot