type Factory<R = any, A extends any[] = any> = (...args: A) => R;
type FactoryParameters<T extends any[]> = { [K in keyof T]: () => T[K] };

export class Container {
  private readonly _registry: Map<() => any, () => any>;

  constructor() {
    this._registry = new Map();
  }

  /**
   * Register a factory and a list of parameters, and return the token under which the factory has been registered.
   */
  register<R, A extends any[]>(factory: Factory<R, A>, and: FactoryParameters<A>): () => R {
    const token = () => {
      const resolvedArguments = and.map((argument) => {
        return this.get(argument);
      });

      return factory(...resolvedArguments as any)
    };

    this._registry.set(token, token);

    return token;
  };

  /**
   * Replace a token by another token.
   */
  replace<R>(token: () => R, by: () => R): void {
    this._registry.set(token, by);
  };

  /**
   * Return the resolved value of a token.
   */
  get<R>(token: () => R): R {
    let registeredToken = this._registry.get(token);

    if (!registeredToken) {
      registeredToken = token;
    }

    return registeredToken();
  }
}

/**
 * Expose convenience singletons.
 */
const container = new Container();

export const register: typeof container.register = container.register.bind(container);
export const replace: typeof container.replace = container.replace.bind(container);
export const get: typeof container.get = container.get.bind(container);
