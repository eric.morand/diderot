import * as tape from "tape";
import {Container, register, get, replace} from "../src";

tape('Diderot', (test) => {
  test.test('Container', (test) => {
    test.test('register', (test) => {
      const container = new Container();

      test.test('MUST return an argument-less function', (test) => {
        const token1 = container.register(() => 1, []);

        test.same(token1(), 1);

        const token2 = container.register((one: number) => one, [() => 2]);

        test.same(token2(), 2);

        const token3 = container.register((one: number) => one + 1, [token2]);

        test.same(token3(), 3);

        test.test('MUST return tokens that resolves their parameters when executed', (test) => {
          const token4 = container.register((one: number) => one + 1, [token3]);

          test.same(token4(), 4, 'MUST be equal to 4');

          container.replace(token3, () => 4);

          test.same(token4(), 5, 'MUST be equal to 5');

          test.end();
        })

        test.end();
      });

      test.end();
    });

    test.test('replace', (test) => {
      const container = new Container();

      const token = container.register(() => 1, []);

      container.replace(token, () => 5);

      test.same(container.get(token), 5, 'MUST replace the token in the container');
      test.same(token(), 1, 'MUST NOT have side effect on the replaced token');

      test.end();
    });

    test.test('get', (test) => {
      const container = new Container();

      const numberToken = container.register(() => 5, []);

      test.same(container.get(numberToken), 5, 'MUST support registered tokens passed as parameter');

      test.same(container.get(() => 5), 5,'MUST support unregistered tokens passed as parameter');

      test.test('MUST resolve registered parameters', (test) => {
        const container = new Container();

        const parameterizedToken = container.register((one: string) => Number.parseInt(one), [
          () => '5'
        ]);

        test.same(container.get(parameterizedToken), 5, 'MUST support functions parameters');

        const stringToken = container.register(() => '5', []);
        const numberToken2 = container.register((one: string) => Number.parseInt(one), [stringToken]);

        test.same(container.get(numberToken2), 5, 'MUST support tokens parameters');

        container.replace(stringToken, container.register(() => '6', []));

        const stringToken2 = container.register((one: string) => Number.parseInt(one), [stringToken]);

        test.same(container.get(stringToken2), 6, 'MUST support overridden tokens parameters');

        test.same(container.get(() => 5), 5,'MUST support unregistered tokens passed as parameter');

        test.end();
      });

      test.end();
    });
  });

  test.test('Singletons', (test) => {
    const token1 = register(() => 5, []);

    test.same(get(token1), 5);

    const token2 = register(() => 6, []);

    replace(token1, token2);

    test.same(get(token1), 6);

    const token3 = register((input: number) => input + 7, [token1]);

    replace(token2, token3);

    test.same(get(token2), 13);

    replace(token3, () => 20);

    test.same(get(token3), 20);

    test.end();
  });
});