import {Container} from "../src";

const container = new Container();

const namedStringToken = container.register(function name() {
  return 'foo';
}, []);

const token = container.register((one: number) => one, [() => 5]);

container.replace(token, container.register((foo: number) => foo, [() =>
  // @ts-expect-error
  '5'
]));

container.register(() => 5, []);
container.replace(namedStringToken,
  // @ts-expect-error
  () => 5
);
container.register(() => 5,
  // @ts-expect-error
  [
    () => 5
  ]
);
container.replace(namedStringToken,
  // @ts-expect-error
  container.register(() => 5, [])
);
container.register(() => 5,
  // @ts-expect-error
  [() => '5']
);

container.register(() => '5', []);
container.replace(namedStringToken, container.register(() => '5', []));
container.register(() => '5',
  // @ts-expect-error
  [() => 5]
);
container.register(namedStringToken, () => '5',
  // @ts-expect-error
  [() => 5]
);
container.register(() => '5',
  // @ts-expect-error
  [() => '5']
);
container.register(namedStringToken, () => '5',
  // @ts-expect-error
  [() => '5']
);

container.register((one: string) => one,
  // @ts-expect-error
  []
);
container.register(namedStringToken, (one: string) => one,
  // @ts-expect-error
  []
);
container.register((one: string) => one, [() =>
  // @ts-expect-error
  5
]);
container.replace(namedStringToken, container.register((one: string) => one, [() =>
  // @ts-expect-error
  5
]));
container.register((one: string) => one, [() => '5']);
container.replace(namedStringToken, container.register((one: string) => one, [() => '5']));

const anonymousStringToken = container.register(() => {
  return 'foo';
}, []);

container.register(() => 5, []);
container.replace(anonymousStringToken,
  // @ts-expect-error
  () => 5
);
container.register(() => 5,
  // @ts-expect-error
  [() => 5]
);
container.replace(anonymousStringToken,
  // @ts-expect-error
  container.register(() => 5, [])
);
container.register(() => 5,
  // @ts-expect-error
  [() => '5']
);

container.register(() => '5', []);
container.replace(anonymousStringToken, container.register(() => '5', []));
container.register(() => '5',
  // @ts-expect-error
  [() => 5]
);
container.replace(anonymousStringToken, () => '5',
  // @ts-expect-error
  [() => 5]
);
container.register(() => '5',
  // @ts-expect-error
  [() => '5'])
;
container.replace(anonymousStringToken, () => '5',
  // @ts-expect-error
  [() => '5']
);

container.register((one: string) => one,
  // @ts-expect-error
  []
);
container.replace(anonymousStringToken, (one: string) => one,
  // @ts-expect-error
  []
);
container.register((one: string) => one, [() =>
  // @ts-expect-error
  5
]);
container.replace(anonymousStringToken, container.register((one: string) => one, [() =>
  // @ts-expect-error
  5
]));
container.register((one: string) => one, [() => '5']);
container.replace(anonymousStringToken, container.register((one: string) => one, [() => '5']));
